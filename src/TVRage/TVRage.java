package TVRage;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import objects.Episode;
import objects.Season;
import objects.Show;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class primarily pulls data from the TVRage API and parses it.
 *
 * @author Nolan Buchanan
 */
public class TVRage {

    /**
     * Queries the TVRage API to search for a show. Returns a HashMap of the possible shows based on
     * the name given and the ids of those shows
     *
     * @param name
     * @return
     */
    public static HashMap searchShow(String name) {
        String url = "http://services.tvrage.com/feeds/search.php?show=" + name;
        HashMap possibilities = new HashMap(); 

        // most of the following is copied from 
        //http://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            URL request = new URL(url);
            InputStream stream = request.openStream();
            Document doc = dBuilder.parse(stream);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("show");

            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) { //not sure what this means
                    Element eElement = (Element) nNode;
                    possibilities.put(eElement.getElementsByTagName("showid").item(0).getTextContent(),
                            eElement.getElementsByTagName("name").item(0).getTextContent() + " "
                            + eElement.getElementsByTagName("started").item(0).getTextContent());
                }
                /*
                 For above do I want to reverse the keys and values? I have no information about
                 the id beforehand so searching the map byt the id is probably pointless                
                 */

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return possibilities;
    }

    /**
     * Pull info about all episodes by the show ID
     *
     * @param id
     * @return
     */
    public static Show getEpisodeList(String id) {
        Show show = new Show();
        Season season = new Season();
        Episode ep = new Episode();

        try {
            String url = "http://services.tvrage.com/feeds/full_show_info.php?sid=" + id;

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            URL request = new URL(url);
            InputStream stream = request.openStream();
            Document doc = dBuilder.parse(stream);

            doc.getDocumentElement().normalize();
            
            // set show info
            show.setTitle(doc.getElementsByTagName("name").item(0).getTextContent());
            show.setID(id);
            show.setStatus(doc.getElementsByTagName("status").item(0).getTextContent());
            String startDate = doc.getElementsByTagName("started").item(0).getTextContent();            
            show.setYear(startDate.substring(startDate.lastIndexOf("/") + 1));

            //loop through seasons
            NodeList nList = doc.getElementsByTagName("Season");

            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                Element eElement = (Element) nNode;
                NodeList lst = eElement.getElementsByTagName("episode");

                season = new Season(Integer.toString(i+1));

                //loop through episodes in season
                for (int j = 0; j < lst.getLength(); j++) {
                    Node nd = lst.item(j);
                    ep = new Episode();

                    if (nd.getNodeType() == Element.ELEMENT_NODE) {
                        Element el = (Element) nd;

                        ep.setTitle(el.getElementsByTagName("title").item(0).getTextContent());
                        ep.setNumber(el.getElementsByTagName("seasonnum").item(0).getTextContent());
                        ep.setAirDate(el.getElementsByTagName("airdate").item(0).getTextContent());
                    }
                                            
                    season.addEpisode(ep);
                }
                                
                show.addSeason(season);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return show;
    }
}
