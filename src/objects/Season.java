
package objects;

import java.util.ArrayList;

/**
 *
 * @author Nolan Buchanan
 */
public class Season {
    String number; 
    ArrayList<Episode> episodes;

    public Season() {
        episodes = new ArrayList<Episode>(5);
    }

    public Season(String number) {
        episodes = new ArrayList<Episode>(5);
        this.number = number;
    }
    
    public String getNumber() {
        return number;
    }

    public ArrayList getEpisodes() {
        return episodes;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setEpisodes(ArrayList episodes) {
        this.episodes = episodes;
    }
    
    public void addEpisode(Episode ep) {
        this.episodes.add(ep);
    }
    
    
    
    
    
}
