
package objects;

import java.io.File;
import java.util.Calendar;

/**
 *
 * @author Nolan Buchanan
 */
public class Episode {
	String title;
	String airDate; // api gives this as yyyy-MM-dd
	String number;
	boolean missing; //If I don't have the file
	int timesWatched;
	Calendar lastWatched;
	File location;
	
	public Episode() {
	}
	
	public Episode(String title, String number) {
		this.title = title;
		this.number = number;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAirDate() {
		return airDate;
	}
	
	public void setAirDate(String airDate) {
		this.airDate = airDate;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	public boolean isMissing() {
		return missing;
	}
	
	public void setMissing(boolean missing) {
		this.missing = missing;
	}
	
	public int getTimesWatched() {
		return timesWatched;
	}
	
	public void setTimesWatched(int timesWatched) {
		this.timesWatched = timesWatched;
	}
	
	public Calendar getLastWatched() {
		return lastWatched;
	}
	
	public void setLastWatched(Calendar lastWatched) {
		this.lastWatched = lastWatched;
	}
	
	public File getLocation() {
		return location;
	}
	
	public void setLocation(File location) {
		this.location = location;
	}
	
	public void setLastWatched(int year, int month, int day) {
		this.lastWatched.set(Calendar.YEAR, year);
		this.lastWatched.set(Calendar.MONTH, month);
		this.lastWatched.set(Calendar.DAY_OF_MONTH, day);
	}
	
	
}
