

package objects;

import java.util.ArrayList;

/**
 *
 * @author Nolan Buchanan
 */
public class Show {
    String title;
    String ID;
    int year;
    String status;
    //String imageURL; //May eventually be useful when implementing a GUI. placeholder as reminder for now
    ArrayList<Season> seasons;
    String[] genres;

    public Show() {
        this.seasons = new ArrayList<Season>();
    }

    public Show(String title, String ID, int year, String status, Season[] seasons, String[] genres) {
        this.title = title;
        this.ID = ID;
        this.year = year;
        this.status = status;
        this.seasons = new ArrayList<Season>();
        this.genres = genres;
    }

    public Show(String title, int year) {
        this.title = title;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList getSeasons() {
        return seasons;
    }

    public void addSeason(Season season) {
        this.seasons.add(season);
    }

    public String[] getGenres() {
        return genres;
    }

    public void setGenres(String[] genres) {
        this.genres = genres;
    }
    
    public int getNumberOfEpisodes() {
        int sum = 0;
        for (int i = 0; i < seasons.size(); i++) {
            sum += seasons.get(i).getEpisodes().size();
        }
        return sum;
    }
    
    
}
