/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtracker;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.InputStream;
import java.net.URL;

/**
 *
 * @author Nolan Buchanan
 */
public class testXMLread {

    /**
     * @param args the command line arguments
     */
   /* public static class dummyClass {

        public boolean readXML() {
            String role1 = null;
            String role2 = null;
            String role3 = null;
            String role4 = null;
            ArrayList<String> rolev;

            rolev = new ArrayList<String>();
            Document dom;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder db = dbf.newDocumentBuilder();
                dom = db.parse("C:\\Users\\Nolan Buchanan\\Documents\\Coding\\Java\\showtracker\\sampleData\\arrow\\showInfo.xml");
                
                System.out.println(dom.getXmlVersion());
                dom.getDocumentElement();
                

                Element doc = dom.getDocumentElement();
                System.out.println(doc.getElementsByTagName("Showinfo"));

                role1 = getTextValue(role1, doc, "role1");
                if (role1 != null) {
                    if (!role1.isEmpty()) {
                        rolev.add(role1);
                        System.out.println(role1);
                    }
                }
                role2 = getTextValue(role2, doc, "role2");
                if (role2 != null) {
                    if (!role2.isEmpty()) {
                        rolev.add(role2);
                    }
                }
                role3 = getTextValue(role3, doc, "role3");
                if (role3 != null) {
                    if (!role3.isEmpty()) {
                        rolev.add(role3);
                    }
                }
                role4 = getTextValue(role4, doc, "role4");
                if (role4 != null) {
                    if (!role4.isEmpty()) {
                        rolev.add(role4);
                    }
                }

                return true;
            } catch (ParserConfigurationException pce) {
                System.out.println(pce.getMessage());
            } catch (SAXException se) {
                System.out.println(se.getMessage());
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
            }

            return false;

        }

        private String getTextValue(String def, Element doc, String tag) {
            String value = def;
            NodeList nl;
            nl = doc.getElementsByTagName(tag);
            if (nl.getLength() > 0 && nl.item(0).hasChildNodes()) {
                value = nl.item(0).getFirstChild().getNodeValue();
            }
            return value;
        }
    }*/
    

    public static void main(String[] args) {
        /*dummyClass a = new dummyClass();
        boolean x = a.readXML();
        System.out.println(x);*/
        
        try {
            File xmlFile = new File("C:\\Users\\Nolan Buchanan\\Documents\\Coding\\Java\\showtracker\\sampleData\\arrow\\showInfo.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            URL url = new URL("http://services.tvrage.com/feeds/showinfo.php?sid=2930");
            InputStream stream = url.openStream();
            Document doc = dBuilder.parse(stream);
            
            doc.getDocumentElement().normalize();
            
            System.out.println("Root element : " + doc.getDocumentElement().getNodeName());
            
            NodeList nList = doc.getElementsByTagName("Showinfo");
            
            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                
                System.out.println("Current Element: " + nNode.getNodeName());
                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    
                    Element eElement = (Element) nNode;
                    
                    System.out.println("id: " + eElement.getElementsByTagName("showid").item(0).getTextContent());
                    System.out.println("showname: " + eElement.getElementsByTagName("showname").item(0).getTextContent());
                    
                    
                }
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
