/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FileHandler;

import Exceptions.SeasonDoesNotExistException;
import java.io.File;
import objects.Episode;
import objects.Season;
import objects.Show;

/**
 *
 * @author Nolan Buchanan
 */
public class FileHandler {
public boolean testing;

public FileHandler(boolean testing) {
	this.testing = testing;
}
    /**
     * Get all shows in the show folder as a list of strings
     * @return 
     */
    
    //TODO pass path to directory or pull from config in function?
    public Show[] getShows() {
	    File showsFolder;
	if (testing) {
		showsFolder = new File ("sampleData\\TV\\");
	} else {
		showsFolder = new File("E:\\Videos\\TV\\"); // Later get this string from a config file
	}
        File[] allShows = showsFolder.listFiles();
        Show[] shows = new Show[allShows.length];
        
        
        for (int i = 0; i < shows.length; i++) {
            int len = allShows[i].getName().length();
            String name = allShows[i].getName();
            // create new show with the name and year constructor
            shows[i] = new Show(name.substring(0, len -7), Integer.parseInt(name.substring(len - 5, len - 1)) );
        }
        
        return shows;
    }
    
    /**
     * Returns all seasons of the given show
     * 
     * @param show
     * @return 
     */
    public Season[] getSeasons(String show, int year) {
        File showFolder; 
	if (testing) {
		showFolder = new File("sampleData\\TV\\" + show + " (" + Integer.toString(year) + ")\\");
	} else {
		showFolder = new File("E:\\Videos\\TV\\" + show + " (" + Integer.toString(year) + ")\\");
	}
        File[] allSeasons = showFolder.listFiles();
        Season[] seasons = new Season[allSeasons.length];
        
        for (int i = 0; i < seasons.length; i++) {
            String name = allSeasons[i].getName();
            seasons[i] = new Season(name.substring(name.length() - 1));            
        }
        
        return seasons;
    }

    /**
     * Returns all seasons of the given show
     * 
     * @param show
     * @return 
     */
    public Season[] getSeasons (Show show) {
	    return getSeasons(show.getTitle(), show.getYear());
    }
    
    // It would be nice if this method could determine year itself
    //TODO need year for folder name but does it need to be passed?
    // glob matching: docs.oracle.com/javase/tutorial/essential/io/find.html

    /**
     * returns an Episode array based on the showname, show year, and season 
     * 
     * @param show
     * @param year
     * @param season
     * @return
     * @throws SeasonDoesNotExistException 
     */
    public Episode[] getEpisodesBySeason(String show, int year, int season) throws SeasonDoesNotExistException {
        //System.out.println("E:\\Videos\\TV\\" + show + "(" + year + ")\\Season " + Integer.toString(season) + "\\");
	File seasonFolder;
	if (testing) {
		seasonFolder = new File("sampleData\\TV\\" + show + " (" + Integer.toString(year) + 
			")\\Season " + Integer.toString(season) + "\\");
	} else {
		seasonFolder = new File("E:\\Videos\\TV\\" + show + " (" + Integer.toString(year) + 
			")\\Season " + Integer.toString(season) + "\\");
	}

	if(!seasonFolder.exists()) {
		throw new SeasonDoesNotExistException("Season " + season + " does not exist for " + show);
	}
	
	    //System.out.println("seasonpath");
	    //System.out.println(seasonFolder.getAbsolutePath());
        File[] allEpisodes = seasonFolder.listFiles();
        Episode[] episodes = new Episode[allEpisodes.length];
        
        for (int i = 0; i < episodes.length; i++) {
            String fileName = allEpisodes[i].getName();
            String epName ; //= fileName.substring(show.length() + 8); // showName.S##E##.epName.extension
	    epName = fileName.split("\\.")[2];
            String epNumber = fileName.substring(show.length() + 5, show.length() + 7);
            episodes[i] = new Episode(epName, epNumber);
	    episodes[i].setLocation(allEpisodes[i]);
	    episodes[i].setMissing(false);
        }
        
        return episodes;
    }

    /**
     * returns an Episode array based on the showname, show year, and season  
     * 
     * @param show
     * @param season
     * @return
     * @throws SeasonDoesNotExistException 
     */
    public Episode[] getEpisodesBySeason(Show show, int season) throws SeasonDoesNotExistException{
	    return getEpisodesBySeason(show.getTitle(), show.getYear(), season);

    }
}


