# README #

This project is intended to help me organize my movie and TV collection. 

### Features ###
(None of these are fully implemented yet)

* Play a random File
* Play the next episode in a series
* Save the current place in a series
* Notify me when a new episode is available
* Display information about the show/movie/episode
* Find and download subtitles
* Automatically organize my media library
* Rename files with the desired format
* Rename files with the correct episode name

This project is currently on hiatus until I can find a reliable data provider. 